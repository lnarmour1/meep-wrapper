# meep container

This is my own wrapper container to make building and running the meep code easier (as a push-button).
The meep-wrapper.sh script is a modified version of the original here under the "Building From Source" section:  
https://meep.readthedocs.io/en/latest/Build_From_Source/

This just provides a docker container in which you can build the meep source tree and run jupyter notebook to play with the meep python examples:  
https://github.com/NanoComp/meep/tree/master/python/examples

### clone meep sources
```
git clone https://github.com/NanoComp/meep.git src/meep
```

### create development environment with all built dependencies
```
docker build -t meep-build-image .
```

### run container
```
docker run \
  -ti \
  -v $PWD/src/meep:/meep/src/meep \
  -v $HOME/.ssh:/root/.ssh \
  -p 0.0.0.0:8888:8888 \
  -p 0.0.0.0:8022:22 \
  --name=meep \
  meep-build-image
```

### build meep itself
```
# inside the container
cd /meep && ./build-meep.sh -n
```

### launch jupyter notebook
```
# inside the container
runjup
```

