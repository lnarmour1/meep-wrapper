from ubuntu:18.04

run apt-get update
run apt-get install -y git lsb-release wget vim sudo

run mkdir -p /meep
workdir /meep

run touch /opt/.2.txt
add build-meep.sh build-meep.sh
run ./build-meep.sh -x
    
run pip3 install jupyter

run apt-get install -y openssh-server

run echo 'alias runjup="jupyter notebook --allow-root --notebook-dir /meep/src/meep/python/examples"' >> /root/.bashrc
run echo 'export PYTHONPATH=/meep/lib/python3.6/site-packages/:/usr/lib/python3.6/site-packages/' >> /root/.bashrc

# ssh for pycharm remote attaching
run mkdir /var/run/sshd
run sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
run sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

add entrypoint.sh /opt/entrypoint.sh

entrypoint ["/opt/entrypoint.sh"]
cmd ["/bin/bash"]
